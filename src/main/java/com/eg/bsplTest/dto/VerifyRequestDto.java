package com.eg.bsplTest.dto;

public class VerifyRequestDto {

	private String number;
	private String userName;
	private String otp;
	
	public VerifyRequestDto(String number, String userName, String otp) {
		super();
		this.number = number;
		this.userName = userName;
		this.otp = otp;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	
}
