package com.eg.bsplTest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eg.bsplTest.dto.UserDataDto;
import com.eg.bsplTest.dto.VerifyRequestDto;
import com.eg.bsplTest.dto.VerifyResponseDto;
import com.eg.bsplTest.entity.ConfirmationToken;
import com.eg.bsplTest.entity.UserData;
import com.eg.bsplTest.service.EmailService;
import com.eg.bsplTest.service.TwilioOTPService;
import com.eg.bsplTest.service.UserService;

@RestController
public class UserRegistrationController {

	@Autowired
	private UserService userService;

	@Autowired
	private TwilioOTPService twilioOTPService;

	@Autowired
	private EmailService emailService;

	@PostMapping("/register")
	public ResponseEntity<?> addUserData(@RequestBody UserDataDto userDto) {
		ResponseEntity response;

		try {
			UserData user = userService.addUser(userDto);

			ConfirmationToken confirmationToken = emailService.sendConfLink(user);

			VerifyResponseDto verifyResponseDto = twilioOTPService.sendOTPforVerification(userDto, confirmationToken);

			return new ResponseEntity<>(confirmationToken, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/varifyOtp")
	public ResponseEntity<?> varifyOtp(@RequestBody VerifyRequestDto verifyRequestDto) {
		String res = twilioOTPService.validateOTP(verifyRequestDto.getOtp(), verifyRequestDto.getNumber());
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@RequestMapping(value = "/confirm-account", method = RequestMethod.POST)
	public String confirmUserAccount(@RequestParam("token") String confirmationToken) {
		String response = emailService.confirmUserAccount(confirmationToken);
		return response;

	}

}
