package com.eg.bsplTest.entity;

import java.util.Date;


import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.eg.bsplTest.dto.OtpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="confirmationToken")
public class ConfirmationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long tokenId;

    private String confirmationToken;

	private OtpStatus status;
	
	private String message;
	
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    
    @OneToOne(targetEntity = UserData.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private UserData userData;

    public ConfirmationToken(UserData userData) {
        this.userData = userData;
        createdDate = new Date();
        confirmationToken = UUID.randomUUID().toString();
    }
   
}