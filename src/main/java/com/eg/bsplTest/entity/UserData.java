package com.eg.bsplTest.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "UserData", uniqueConstraints = { @UniqueConstraint(columnNames = { "mobileNumber", "emailId" }) })
public class UserData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;

	@Size(min = 10, max = 17, message = "Number should have at least 10 or less than 17 digits")
	private String mobileNumber;

	private String name;

	private String address;

	private String emailId;

	private String password;

	private boolean isEnabled;

}
