package com.eg.bsplTest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eg.bsplTest.dto.UserDataDto;
import com.eg.bsplTest.entity.UserData;
import com.eg.bsplTest.repository.UserDataRepository;

@Service
public class UserService {
	
	@Autowired
	private UserDataRepository userRepository;

	@Autowired
	private OtpService otpService;
	
	public UserData addUser(UserDataDto userDto) {
		
		UserData user = new UserData();
		user.setName(userDto.getName());
		user.setAddress(userDto.getAddress());
		user.setEmailId(userDto.getEmailId());
		user.setMobileNumber(userDto.getMobileNumber());
		user.setPassword(userDto.getPassword());
		
		userRepository.save(user);
		
		return user;
	}
	
	public void sendOtp(String mobileNo) {
		int otp = otpService.generateOTP(mobileNo);
		
		Map<String,String> replacements = new HashMap<String,String>();
		replacements.put("mobile", mobileNo);
		replacements.put("otpnum", String.valueOf(otp));

	}
}
