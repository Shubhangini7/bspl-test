	package com.eg.bsplTest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.eg.bsplTest.entity.ConfirmationToken;
import com.eg.bsplTest.entity.UserData;
import com.eg.bsplTest.repository.ConfirmationTokenRepository;
import com.eg.bsplTest.repository.UserDataRepository;

@Service
public class EmailService {

	private JavaMailSender javaMailSender;
	
	@Autowired
	public EmailService (JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	
	@Async
	public void sendEmail(SimpleMailMessage email) {
		javaMailSender.send(email);
	}
	
	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;
	
	@Autowired
	private UserDataRepository userRepository;

	public ConfirmationToken sendConfLink(UserData user) {
		ConfirmationToken confirmationToken = new ConfirmationToken(user);

		confirmationTokenRepository.save(confirmationToken);

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(user.getEmailId());
		System.out.println("mail id is : " + user.getEmailId());
		mailMessage.setFrom("testbsplregapi@gmail.com");
		mailMessage.setSubject("complete Registration!");
		mailMessage.setText("To Confirm your account, please click here : "
				+ "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken());
		try {
			sendEmail(mailMessage);
			System.out.println("mail send by: " + mailMessage.getFrom());
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("faild");
		}
		return confirmationToken;
	}

	public String confirmUserAccount(String confirmationToken) {
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
		String status;
		if (token != null) {
			UserData user = userRepository.findByEmailIdIgnoreCase(token.getUserData().getEmailId());
			user.setEnabled(true);
			userRepository.save(user);
			status = "account Verified";
		} else {
			status = "the link is invalid or broken!";
		}
		return status;

	}

}
