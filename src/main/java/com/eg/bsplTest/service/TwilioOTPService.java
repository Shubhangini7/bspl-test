package com.eg.bsplTest.service;

import java.text.DecimalFormat;

import com.twilio.rest.api.v2010.account.ValidationRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eg.bsplTest.dto.OtpStatus;
import com.eg.bsplTest.dto.UserDataDto;
import com.eg.bsplTest.dto.VerifyRequestDto;
import com.eg.bsplTest.dto.VerifyResponseDto;
import com.eg.bsplTest.entity.ConfirmationToken;
import com.eg.bsplTest.entity.TwilioConfig;
import com.eg.bsplTest.entity.UserData;
import com.eg.bsplTest.repository.ConfirmationTokenRepository;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import ch.qos.logback.classic.Logger;

@Service
public class TwilioOTPService {

	@Autowired
	private TwilioConfig twilioConfig;

	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;

	Map<String, String> otpMap = new HashMap<String, String>();

	public VerifyResponseDto sendOTPforVerification(UserDataDto userDto, ConfirmationToken confirmationToken) {

		VerifyResponseDto verifyResponseDto;

		try {

			PhoneNumber to = new PhoneNumber(userDto.getMobileNumber());
			PhoneNumber from = new PhoneNumber(twilioConfig.getTrialNumber());

			String otp = generateOTP();
			String otpMsg = "Dear user you OTP is " + otp + " please use to complete varification.";

			Message message = Message.creator(to, from, otpMsg).create();

			otpMap.put(userDto.getMobileNumber(), otp);

			verifyResponseDto = new VerifyResponseDto(OtpStatus.DELIVERED, otpMsg);
			
			confirmationToken.setStatus(OtpStatus.DELIVERED);
			confirmationToken.setMessage(otp);
			
			

		} catch (Exception e) {

			verifyResponseDto = new VerifyResponseDto(OtpStatus.FAILED, e.getMessage());
			System.out.println(e.getMessage());

		}

		return verifyResponseDto;
	}

	// 6 digit otp
	private String generateOTP() {
		return new DecimalFormat("000000").format(new Random().nextInt(999999));
	}

	public String validateOTP(String userInputOtp, String mobileNumber) {
		String status = "";
		if (userInputOtp.equals(otpMap.get(mobileNumber))) {
			status = "Valid Otp";
			System.out.println(status);
		} else {
			status = "Invalid otp please retry !";
			System.out.println(status);
		}
		return status;
	}

}
