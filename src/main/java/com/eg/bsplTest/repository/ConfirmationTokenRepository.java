package com.eg.bsplTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eg.bsplTest.entity.ConfirmationToken;

@Repository
public interface ConfirmationTokenRepository  extends JpaRepository<ConfirmationToken, String> {
    ConfirmationToken findByConfirmationToken(String confirmationToken);
    }