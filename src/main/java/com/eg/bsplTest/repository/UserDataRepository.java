package com.eg.bsplTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eg.bsplTest.entity.UserData;

@Repository
public interface UserDataRepository extends JpaRepository<UserData, Long>{
    UserData findByEmailIdIgnoreCase(String emailId);
}
